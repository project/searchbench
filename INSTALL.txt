-------------
Requirements:
-------------
 - PHP 4.3.0 or later.
     http://www.php.net/downloads.php

 - Drupal 5.x
     http://drupal.org/download


-------------
Installation:
-------------
1) Enable the searchbench module.

   Visit "Administer >> Site building >> Modules" and enable the "searchbench"
   module.

