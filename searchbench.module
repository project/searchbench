<?php

/**
 * @file
 * Provides tools for benchmarking Drupal's search engine.
 *
 * Copyright (c) 2008.
 *   Jeremy Andrews <jeremy@tag1consulting.com>.
 */

define(SEARCHBENCH_1_10, -1);
define(SEARCHBENCH_1_5, -2);
define(SEARCHBENCH_3_7, -3);
define(SEARCHBENCH_5_10, -4);

define(SEARCHBENCH_MIN_SEARCHES, 100);
define(SEARCHBENCH_MAX_SEARCHES, 50000);

/**
 * Drupal hook_menu() implementation.
 */
function searchbench_menu($may_cache) {
  $items = array();

  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/searchbench',
      'title' => t('Search Benchmark'),
      'description' => t('Configure tools for benchmarking search'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('searchbench_admin_settings'),
      'access' => user_access('access administration pages'),
    );
    $items[] = array(
      'path' => 'searchbench',
      'title' => t('Search benchmark'),
      'description' => t('Perform search benchmarks'),
      'callback' => t('drupal_get_form'),
      'callback arguments' => array('searchbench_benchmark'),
      'access' => user_access('perform search benchmarks'),
    );
    $items[] = array(
      'path' => 'searchbench/report',
      'title' => t('Reports'),
      'description' => t('Report on search benchmark results'),
      'callback' => t('drupal_get_form'),
      'callback arguments' => array('searchbench_report'),
      'access' => user_access('perform search benchmarks'),
    );
  }
  else {
    $report = (int)arg(2);
    if ($report) {
      $items[] = array(
        'path' => 'searchbench/report',
        'title' => t('Reports'),
        'description' => t('Display search benchmark report'),
        'callback' => t('searchbench_report_display'),
        'callback arguments' => array($report),
        'access' => user_access('perform search benchmarks'),
      );
    }
  }
 
  return $items;
}

/**
 * Drupal hook_perm() implementation.
 */
function searchbench_perm() {
  return array('perform search benchmarks');
}

/**
 * Perform search benchmarks.
 */
function searchbench_benchmark() {
  $form = array();

  $result = db_query('SELECT did, name FROM {searchbench_searchlist_data} ORDER BY name ASC');
  while ($data = db_fetch_object($result)) {
    $options[$data->did] = $data->name;
  }

  if (empty($options)) {
    drupal_set_message('You must first create a search list.');
    drupal_goto('admin/settings/searchbench');
  }

  $form['search'] = array(
    '#type' => 'select',
    '#title' => t('Search'),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => $_SESSION['searchbench_search'],
  );

  $form['repeat'] = array(
    '#type' => 'select',
    '#title' => t('Repeat'),
    '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 50, 100)),
    '#required' => TRUE,
    '#description' => t('How many times to repeat the above test.'),
    '#default_value' => $_SESSION['searchbench_repeat'],
  );

  $form['target'] = array(
    '#type' => 'textfield',
    '#title' => t('Target URL'),
    '#size' => 32,
    '#description' => t('Complete URL to search page.  For example: http://sample.com/search/node'),
    '#required' => TRUE,
    '#default_value' => $_SESSION['searchbench_target'],
  );

  $form['log'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log search data'),
    '#default_value' => $_SESSION['searchbench_log'],
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Search name'),
    '#size' => 32,
    '#description' => t('If logging your search data, optionally give the search a reference name.'),
    '#default_value' => $_SESSION['searchbench_name'],
  );


  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Perform search benchmark'),
  );

  return $form;
}

/**
 * Validate benchmark settings.
 */
function searchbench_benchmark_validate($form_id, $form_values) {
  if (!valid_url($form_values['target'])) {
    form_set_error('target', t('You must enter a valid url.'));
  }
}

/**
 * Perform actual benchmarks.
 */
function searchbench_benchmark_submit($form_id, $form_values) {
  $_SESSION['searchbench_search'] = $form_values['search'];
  $_SESSION['searchbench_repeat'] = $form_values['repeat'];
  $_SESSION['searchbench_target'] = $form_values['target'];
  $_SESSION['searchbench_log'] = $form_values['log'];
  $_SESSION['searchbench_name'] = $form_values['name'];
  $base = $form_values['target'];

  // Prepare to log search data.
  if ($form_values['log']) {
    if (!$form_values['name']) {
      // TODO: Create a reasonably useful name based on search performed.
      $form_values['name'] = t('unnamed');
    }
    // If name was already used, delete the old test results.
    $sldid = db_result(db_query("SELECT sldid FROM {searchbench_searchlog_data} WHERE name = '%s'", $form_values['name']));
    if ($sldid) {
      drupal_set_message(t('Purging old "%name" search results.', array('%name' => $form_values['name'])));
      db_query('DELETE FROM {searchbench_searchlog} WHERE sldid = %d', $sldid);
      db_query('DELETE FROM {searchbench_searchlog_data} WHERE sldid = %d', $sldid);
    }
    // Prepare new search list, generate SearchLog DataID (sldid).
    db_query("INSERT INTO {searchbench_searchlog_data} (sldid, name) VALUES(%d, '%s')", $sldid, $form_values['name']);
    $sldid = db_result(db_query("SELECT sldid FROM {searchbench_searchlog_data} WHERE name = '%s'", $form_values['name']));
    if (!$sldid) {
      drupal_set_message('Unexpected error, failed to set up search log.', 'error');
      return -1;
    }
  }

  $searches = $elapsed_time = array();
  $total_searches = $total_elapsed_time = $errors = 0;

  $i = 0;
  $total_start = _searchbench_timer();
  while ($i < $form_values['repeat']) {
    $result = db_query('SELECT did, qid, query FROM {searchbench_searchlist} WHERE did = %d ORDER BY qid ASC', (int)$form_values['search']);
    $test_start = _searchbench_timer();
    while ($search = db_fetch_object($result)) {
      $url = $base . htmlentities(str_replace(' ', '+', "/$search->query"), ENT_NOQUOTES);
      $query_start = _searchbench_timer();
      if (FALSE === file_get_contents($url)) {
        $error = 1;
        $errors++;
      }
      else {
        $error = 0;
      }
      $search_time = _searchbench_timer($query_start);
      if ($form_values['log']) {
        db_query('INSERT INTO {searchbench_searchlog} (sldid, did, qid, number, error, time) VALUES(%d, %d, %d, %d, %d, %f)', $sldid, $search->did, $search->qid, $i, $error, $search_time);
      }
      $searches[$i]++;
      $total_searches++;
      $elapsed_time[$i] += $search_time;
      $total_elapsed_time += $search_time;
    }
    // This timer includes logging overhead if logging is enabled.
    $test_timers[$i++] = _searchbench_timer($test_start);
  }
  $timer = _searchbench_timer($total_start);
  drupal_set_message(t('Performed %total search tests in %time seconds.', array('%total' => $total_searches, '%time' => $total_elapsed_time)));
  drupal_set_message(t('Average time per search query: %average seconds.', array('%average' => $total_elapsed_time / $total_searches)));
}

/**
 * Generate reports from logged search results.
 */
function searchbench_report() {
  $form = array();

  $result = db_query('SELECT sldid, name FROM {searchbench_searchlog_data} ORDER BY sldid DESC');
  $logs = array();
  while ($searchlog = db_fetch_object($result)) {
    $logs[$searchlog->sldid] = $searchlog->name;
  }

  if (empty($logs)) {
    drupal_set_message('You must first log some searches.');
    drupal_goto('searchbench');
  }

  $form['log'] = array(
    '#type' => 'select',
    '#title' => t('Logged searches'),
    '#options' => $logs,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Generate report'),
  );

  return $form;
}

/**
 * Redirect to appropriate report page.
 */
function searchbench_report_submit($form_id, $form_values) {
  drupal_goto('searchbench/report/'. $form_values['log']);
}

/**
 * Analyze the search logs, generate useful report.
 */
function searchbench_report_display($sldid) {
  drupal_set_title(db_result(db_query('SELECT name FROM {searchbench_searchlog_data} WHERE sldid = %d', $sldid)));
  $output = '';

  // # of tests
  $tests = db_result(db_query('SELECT MAX(number) FROM {searchbench_searchlog} WHERE sldid = %d', $sldid));
  // time for all tests
  $total_time = db_result(db_query('SELECT SUM(time) FROM {searchbench_searchlog} WHERE sldid = %d', $sldid));
  // # of searches per test
  $searches = db_result(db_query('SELECT MAX(qid) FROM {searchbench_searchlog} WHERE sldid = %d AND number = 0', $sldid));

  $rows = array(
    array(
      array('data' => t('Total tests'), 'header' => TRUE),
      $tests + 1,
    ),
    array(
      array('data' => t('Searches per test'), 'header' => TRUE),
      $searches + 1,
    ),
    array(
      array('data' => t('Total time'), 'header' => TRUE),
      t('!time seconds', array('!time' => number_format(round($total_time, 4), 4))),
    ),
    array(
      array('data' => t('Average time per test'), 'header' => TRUE),
      t('!time seconds', array('!time' => number_format(round($total_time / ($tests + 1), 4), 4))),
    ),
    array(
      array('data' => t('Average time per query'), 'header' => TRUE),
      t('!time seconds', array('!time' => number_format(round($total_time / ($tests + 1) / ($searches + 1), 4), 4))),
    ),
    array(NULL, NULL),
  );

  $i = 0;
  while ($i <= $tests) {
    $time = db_result(db_query('SELECT SUM(time) FROM {searchbench_searchlog} WHERE sldid = %d AND number = %d', $sldid, $i));
    $rows[] = array(
      array('data' => t('Average time for test #!number', array('!number' => $i + 1)), 'header' => TRUE),
      t('!time seconds', array('!time' => number_format(round($time, 4), 4))));
    $rows[] = array(
      t('&nbsp;Average time per query for test #!number', array('!number' => $i + 1)),
      t('!time seconds', array('!time' => number_format(round($time / ($searches + 1), 4), 4))));
    $i++;
  }

  $rows[] = array(
    NULL,
    NULL);


  $result = db_query('SELECT did, qid FROM {searchbench_searchlog} WHERE sldid = %d AND number = 0 ORDER BY qid ASC', $sldid);
  while ($query = db_fetch_object($result)) {
    $rows[] = array(
      array('data' => t('Query #!query', array('!query' => $query->qid + 1)), 'header' => TRUE),
      NULL);
    $details = db_result(db_query('SELECT query FROM {searchbench_searchlist} WHERE did = %d AND qid = %d', $query->did, $query->qid));
    $rows[] = array(
      "&nbsp;$details",
      NULL);
    $queries = db_query('SELECT number, error, time FROM {searchbench_searchlog} WHERE sldid = %d AND qid = %d ORDER BY number ASC', $sldid, $query->qid);
    while ($log = db_fetch_object($queries)) {
      $rows[] = array(
        t('&nbsp;&nbsp;&nbsp;Test #!test', array('!test' => $log->number + 1)),
        t('!time seconds', array('!time' => number_format(round($log->time, 5), 5))));
    }
  }
  $output = theme('table', array(), $rows, array('class' => 'searchbench-details'));
  return $output;
}

/**
 * Helper function, time searches.
 */
function _searchbench_timer($time = 0) {
  if (!$time) {
    return _searchbench_microtime();
  }
  else {
    return _searchbench_microtime() - $time;
  }
}

/**
 * Helper function, construct usable microtime.
 */
function _searchbench_microtime() {
  $microtime = microtime();
  $microtime = explode(' ', $microtime);
  return $microtime[1] + $microtime[0];
}

/**
 * SearchBench configuration.
 */
function searchbench_admin_settings() {
  $form = array();

  $count = db_result(db_query('SELECT COUNT(*) FROM {searchbench_wordlist}'));
  $form['wordlist'] = array(
    '#type' => 'fieldset',
    '#title' => t('Wordlist'),
    '#collapsible' => TRUE,
    '#collapsed' => $count ? TRUE : FALSE,
  );
  $form['wordlist']['status'] = array(
    '#value' => t('Words in word list: %count', array('%count' => number_format($count))),
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );
  $form['wordlist']['populate'] = array(
    '#type' => 'submit',
    '#value' => t('Populate wordlist'),
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );

  $count = db_result(db_query('SELECT COUNT(*) FROM {searchbench_searchlist_data}'));
  $form['searchlist'] = array(
    '#type' => 'fieldset',
    '#title' => t('Searchlists'),
    '#collapsible' => TRUE,
  );
  $form['searchlist']['status'] = array(
    '#value' => t('Generated search lists: %count', array('%count' => number_format($count))),
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );
  $options = drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
  $options[SEARCHBENCH_1_10] = t('Random (1-10)');
  $options[SEARCHBENCH_1_5] = t('Random (1-5)');
  $options[SEARCHBENCH_3_7] = t('Random (3-7)');
  $options[SEARCHBENCH_5_10] = t('Random (5-10)');
  $form['searchlist']['words'] = array(
    '#type' => 'select',
    '#title' => t('Words per search'),
    '#options' => $options,
    '#default_value' => $_SESSION['searchbench_words'],
    '#description' => t('How many search terms to include in each search query.'),
  );
  $options = drupal_map_assoc(array(10, 50, 100, 500, 1000, 5000, 10000, 50000));
  $options[0] = t('Random');
  $form['searchlist']['searches'] = array(
    '#type' => 'select',
    '#title' => t('Total searches to perform'),
    '#options' => $options,
    '#default_value' => $_SESSION['searchbench_searches'],
    '#description' => t('How many unique search queries should be generated.'),
  );

  $form['searchlist']['phrases'] = array(
    '#type' => 'radios',
    '#title' => t('Search phrases'),
    '#options' => array(t('Disabled'), t('Random')),
    '#default_value' => $_SESSION['searchbench_phrases'],
    '#description' => t('If set to random, some search terms will be wrapped in quotes.'),
  );
  $form['searchlist']['negation'] = array(
    '#type' => 'radios',
    '#title' => t('Negate search terms'),
    '#options' => array(t('Disabled'), t('Random')),
    '#default_value' => $_SESSION['searchbench_negation'],
    '#description' => t('If set to random, some search terms will be preceded by the minus sign.'),
  );
  $form['searchlist']['generate'] = array(
    '#type' => 'submit',
    '#value' => t('Generate search list'),
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );

  return $form;
}

/**
 * Validate settings.
 */
function searchbench_admin_settings_validate($form_id, $form_values) {
  if ($form_values['op'] == t('Generate search list')) {
    $count = db_result(db_query('SELECT COUNT(*) FROM {searchbench_wordlist}'));
    if (!$count) {
      form_set_error('populate', t('You must populate the wordlist before you can generate a search list.'));
    }
  }
}

/**
 * Save settings.
 */
function searchbench_admin_settings_submit($form_id, $form_values) {
  if ($form_values['op'] == t('Populate wordlist')) {
    searchbench_download_wordlists(TRUE);
  }
  if ($form_values['op'] == t('Generate search list')) {
    if (!$form_values['name']) {
      if (!$form_values['searches']) {
        $name = t('Random number of searches, ');
      }
      else {
        $name = t('!num searches, ', array('!num' => $form_values['searches']));
      }
      switch ($form_values['words']) {
        case SEARCHBENCH_1_10:
          $name .= t('one to ten words per search');
          break;
        case SEARCHBENCH_1_5:
          $name .= t('one to five words per search');
          break;
        case SEARCHBENCH_3_7:
          $name .= t('three to seven words per search');
          break;
        case SEARCHBENCH_5_10:
          $name .= t('five to ten words per search');
          break;
        default:
          $name .= t('!num words per search', array('!num' => $form_values['words']));
          break;
      }
    }
    searchbench_generate_searchlist($form_values['words'], $form_values['searches'], $form_values['negation'], $form_values['phrases'], $name);
  }
}

/**
 * Automatically populate wordlist table with some wordlists found online.
 */
function searchbench_download_wordlists() {
  // URLs of online wordlists
  // TODO: Make this configurable.
  $urls = array(
    'http://packetstormsecurity.org/Crackers/wordlists/common-1',
    'http://packetstormsecurity.org/Crackers/wordlists/common-2',
    'http://packetstormsecurity.org/Crackers/wordlists/common-3',
    'http://packetstormsecurity.org/Crackers/wordlists/common-4',
    'http://tag1consulting.com/files/drupal_wordlist.txt',
  );
  $total_word_count = 0;

  foreach ($urls as $url) {
    $hash = md5($url);
    if (!variable_get("searchbench-$hash", '')) {
      $word_count = 0;
      // load list into an array.
      $words = file($url);
  
      if (is_array($words) && !empty($words)) {
        $line = 0;
        while (!empty($words[$line])) {
          list($word, $junk) = split("\t", $words[$line++]);
          $word = check_plain(trim(utf8_encode(strtolower($word))));
          if (strlen($word)) {
            $wid = db_result(db_query("SELECT wid FROM {searchbench_wordlist} WHERE word = '%s'", $word));
            if (!$wid) {
              db_query("INSERT INTO {searchbench_wordlist} (word) VALUES('%s')", $word);
              $word_count++;
              $total_word_count++;
            }
          }
        }
        if ($word_count) {
          // Note once we've succesfully downloaded a wordlist so we don't try
          // again.
          variable_set("searchbench-$hash", time());
        }
        drupal_set_message(t('Added %num new words from %url.', array('%num' => number_format($word_count), '%url' => $url)));
      }
      else {
        drupal_set_message(t('Failed to retrieve wordlist from !url.', array('!url' => l($url, $url))), 'error');
      }
    }
    else {
      drupal_set_message(t('Skipping !url, already downloaded.', array('!url' => l($url, $url))));
    }
  }
  drupal_set_message(t('Added a total of %num new words.', array('%num' => number_format($total_word_count))));
}

/**
 * Generate a search list (allows same identical test to be performed
 * multiple times).
 */
function searchbench_generate_searchlist($words, $searches, $negation, $phrases, $name) {
  $_SESSION['searchbench_words'] = $words;
  $_SESSION['searchbench_searches'] = $searches;
  $_SESSION['searchbench_phrases'] = $phrases;
  $_SESSION['searchbench_negation'] = $negation;

  // If creating new search list by same name, delete old list.
  $did = db_result(db_query("SELECT did FROM {searchbench_searchlist_data} WHERE name = '%s'", $name));
  if ($did) {
    drupal_set_message(t('Purging old "%name" wordlist.', array('%name' => $name)));
    db_query('DELETE FROM {searchbench_searchlist} WHERE did = %d', $did);
    db_query('DELETE FROM {searchbench_searchlist_data} WHERE did = %d', $did);
  }
  // Prepare new search list, generate DataID (did).
  db_query("INSERT INTO {searchbench_searchlist_data} (did, name) VALUES(%d, '%s')", $did, $name);
  $did = db_result(db_query("SELECT did FROM {searchbench_searchlist_data} WHERE name = '%s'", $name));
  if (!$did) {
    drupal_set_message('Unexpected error, unable to create new search list.', 'error');
    return -1;
  }

  if (!$searches) {
    $searches = rand(SEARCHBENCH_MIN_SEARCHES, SEARCHBENCH_MAX_SEARCHES);
  }

  $i = 0;
  $queries = array();
  while ($i < $searches) {
    switch ($words) {
      case SEARCHBENCH_1_10:
        $number = rand(1, 10);
        break;
      case SEARCHBENCH_1_5:
        $number = rand(1, 5);
        break;
      case SEARCHBENCH_3_7:
        $number = rand(3, 7);
        break;
      case SEARCHBENCH_5_10:
        $number = rand(5, 10);
        break;
      default:
        $number = $words;
        break;
    }

    $result = db_query('SELECT word FROM {searchbench_wordlist} ORDER BY RAND() LIMIT %d', $number);
    $query = array();
    $in_phrase = FALSE;
    while ($word = db_fetch_object($result)) {
      if ($negation && !rand(0,3)) {
        $negate = '-';
      }
      else {
        $negate = '';
      }
      if ($phrases && !rand(0,2)) {
        if ($in_phrase) {
          $query[] = "$word->word\"";
          $in_phrase = FALSE;
        }
        else {
          $query[] = "$negate\"$word->word";
          $in_phrase = TRUE;
        }
      }
      else {
        if ($in_phrase) {
          $query[] = "$word->word";
        }
        else {
          $query[] = "$negate$word->word";
        }
      }
    }
    // Close phrase if open.
    if ($in_phrase) {
      $last = array_pop($query);
      array_push($query, "$last\"");
    }
    db_query("INSERT INTO {searchbench_searchlist} (qid, did, query) VALUES(%d, %d, '%s')", $i, $did, implode(' ', $query));
    $i++;
  }
  drupal_set_message(t('Generated searchlist named "%name".', array('%name' => $name)));
}
