---------
Overview:
---------
The SearchBench module provides tools for benchmarking Drupal's searching
functionality.

Written by:    Jeremy Andrews <jeremy@tag1consulting.com>
Maintained by: Jeremy Andrews <jeremy@tag1consulting.com>


-------------
Installation:
-------------
Please see INSTALL.txt.
